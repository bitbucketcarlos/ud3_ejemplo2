package com.example.ud3_ejemplo2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    // Método (Función) que crea un Intent explícito para abrir la Actividad 1
    fun abrirActividad1(view: View) {
        val intentAct1 = Intent(this, Actividad1::class.java)
        startActivity(intentAct1)
    }

    // Método (Función) que crea un Intent explícito para abrir la Actividad 2
    fun abrirActividad2(view: View) {
        val intentAct2 = Intent(this, Actividad2::class.java)
        startActivity(intentAct2)
    }
}